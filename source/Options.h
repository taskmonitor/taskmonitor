/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     Options class
 * @details   Runtime options with defaults
 *-
 */

#pragma once

#include <any>
#include <string>

#include "Defaults.h"

#include "../bswinfra/source/KeyFile.h"

namespace tkm::monitor
{

class Options
{
public:
  enum class Key {
    RuntimeDirectory,
    LogLevel,
    ContainersPath,
    CGroupsContextPath,
    RxBufferSize,
    TxBufferSize,
    MsgBufferSize,
    EnableProcFDCount,
    ProdModeFastLaneInt,
    ProdModePaceLaneInt,
    ProdModeSlowLaneInt,
    ProfModeFastLaneInt,
    ProfModePaceLaneInt,
    ProfModeSlowLaneInt,
    ProfModeIfPath,
    SelfLowerPriority,
    ReadProcAtInit,
#ifdef WITH_PROC_EVENT
    EnableProcEvent,
    UpdateOnProcEvent,
#endif
#ifdef WITH_PROC_ACCT
    EnableProcAcct,
#endif
#ifdef WITH_STARTUP_DATA
    EnableStartupData,
    StartupDataCleanupTime,
#endif
#ifdef WITH_VM_STAT
    EnableSysProcVMStat,
#endif
#ifdef WITH_JOB_CONTROL
    EnableJobControl,
    JCDefaultSuspendTimeout,
    JCTriggerGroupOnSignal,
    JCSignalDestination,
    JCSignalPath,
    JCSignalInterface,
    JCSignalEventName,
#endif
#ifdef WITH_WAKE_LOCK
    TCPActiveWakeLock,
#endif
    EnableTCPServer,
    TCPServerAddress,
    TCPServerPort,
    CollectorInactiveTimeout,
    EnableUDSServer,
    UDSServerSocketPath,
    UDSMonitorCollectorInactivity,
  };

public:
  explicit Options(const std::string &configFile);

  auto getFor(Key key) -> std::string const;
  bool hasConfigFile() { return m_configFile != nullptr; }
  auto getConfigFile() -> std::shared_ptr<bswi::kf::KeyFile> { return m_configFile; }

private:
  auto getStringOrDefault(const std::string &section,
                          const std::string &prop,
                          Defaults::Default defval) -> const std::string;
  auto getStringOrDefaultValidateInterval(const std::string &section,
                                          const std::string &prop,
                                          Defaults::Default defval,
                                          long min,
                                          long max) -> const std::string;

private:
  std::shared_ptr<bswi::kf::KeyFile> m_configFile = nullptr;
};

} // namespace tkm::monitor
