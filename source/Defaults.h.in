/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     Defaults class
 * @details   Declare defaults values for application
 *-
 */

#pragma once

#include <map>
#include <string>

namespace tkm::monitor
{

class Defaults
{
public:
  enum class Default {
    Version,
    ConfPath,
    LogLevel,
    ContainersPath,
    CGroupsContextPath,
    RuntimeDirectory,
    RxBufferSize,
    TxBufferSize,
    MsgBufferSize,
    EnableProcFDCount,
    ProdModeFastLaneInt,
    ProdModePaceLaneInt,
    ProdModeSlowLaneInt,
    ProfModeFastLaneInt,
    ProfModePaceLaneInt,
    ProfModeSlowLaneInt,
    ProfModeIfPath,
    SelfLowerPriority,
    ReadProcAtInit,
#ifdef WITH_PROC_EVENT
    EnableProcEvent,
    UpdateOnProcEvent,
#endif
#ifdef WITH_PROC_ACCT
    EnableProcAcct,
#endif
#ifdef WITH_STARTUP_DATA
    EnableStartupData,
    StartupDataCleanupTime,
#endif
#ifdef WITH_VM_STAT
    EnableSysProcVMStat,
#endif
#ifdef WITH_JOB_CONTROL
    EnableJobControl,
    JCDefaultSuspendTimeout,
    JCTriggerGroupOnSignal,
    JCSignalDestination,
    JCSignalPath,
    JCSignalInterface,
    JCSignalEventName,
#endif
#ifdef WITH_WAKE_LOCK
    TCPActiveWakeLock,
#endif
    EnableTCPServer,
    TCPServerAddress,
    TCPServerPort,
    CollectorInactiveTimeout,
    EnableUDSServer,
    UDSServerSocketPath,
    UDSMonitorCollectorInactivity,
  };

  enum class Val { True, False, None, ProcAcct, ProcInfo };
  enum class Arg { WithEventSource };

  Defaults()
  {
    m_table.insert(std::pair<Default, std::string>(Default::Version, "@PROJECT_VERSION@"));
    m_table.insert(std::pair<Default, std::string>(Default::ConfPath, "@TKM_CONFIG_FILE@"));
    m_table.insert(std::pair<Default, std::string>(Default::LogLevel, "info"));
#ifdef WITH_LXC
    m_table.insert(std::pair<Default, std::string>(Default::ContainersPath, "/var/lib/lxc"));
#endif
#ifdef WITH_CGROUPS_CONTEXT
    m_table.insert(std::pair<Default, std::string>(Default::CGroupsContextPath, "/lxc"));
#endif
    m_table.insert(std::pair<Default, std::string>(Default::RuntimeDirectory, "/run/taskmonitor"));
    m_table.insert(std::pair<Default, std::string>(Default::RxBufferSize, "524288"));
    m_table.insert(std::pair<Default, std::string>(Default::TxBufferSize, "524288"));
    m_table.insert(std::pair<Default, std::string>(Default::MsgBufferSize, "1048576"));
    m_table.insert(std::pair<Default, std::string>(Default::ProdModeFastLaneInt, "10000000"));
    m_table.insert(std::pair<Default, std::string>(Default::ProdModePaceLaneInt, "30000000"));
    m_table.insert(std::pair<Default, std::string>(Default::ProdModeSlowLaneInt, "60000000"));
    m_table.insert(std::pair<Default, std::string>(Default::ProfModeFastLaneInt, "1000000"));
    m_table.insert(std::pair<Default, std::string>(Default::ProfModePaceLaneInt, "5000000"));
    m_table.insert(std::pair<Default, std::string>(Default::ProfModeSlowLaneInt, "10000000"));
    m_table.insert(std::pair<Default, std::string>(Default::ProfModeIfPath, "none"));
    m_table.insert(std::pair<Default, std::string>(Default::SelfLowerPriority, "true"));
    m_table.insert(std::pair<Default, std::string>(Default::ReadProcAtInit, "true"));
#ifdef WITH_PROC_EVENT
    m_table.insert(std::pair<Default, std::string>(Default::EnableProcEvent, "true"));
    m_table.insert(std::pair<Default, std::string>(Default::UpdateOnProcEvent, "true"));
#endif
#ifdef WITH_PROC_ACCT
    m_table.insert(std::pair<Default, std::string>(Default::EnableProcAcct, "true"));
#endif
#ifdef WITH_STARTUP_DATA
    m_table.insert(std::pair<Default, std::string>(Default::EnableStartupData, "false"));
    m_table.insert(std::pair<Default, std::string>(Default::StartupDataCleanupTime, "60000000"));
#endif
    m_table.insert(std::pair<Default, std::string>(Default::EnableTCPServer, "true"));
    m_table.insert(std::pair<Default, std::string>(Default::TCPServerAddress, "localhost"));
    m_table.insert(std::pair<Default, std::string>(Default::TCPServerPort, "3357"));
    m_table.insert(std::pair<Default, std::string>(Default::CollectorInactiveTimeout, "10000000"));
    m_table.insert(std::pair<Default, std::string>(Default::EnableUDSServer, "false"));
    m_table.insert(std::pair<Default, std::string>(Default::UDSServerSocketPath,
                                                   "/run/taskmonitor/taskmonitor.sock"));
    m_table.insert(
        std::pair<Default, std::string>(Default::UDSMonitorCollectorInactivity, "false"));
    m_table.insert(std::pair<Default, std::string>(Default::EnableProcFDCount, "false"));
#ifdef WITH_VM_STAT
    m_table.insert(std::pair<Default, std::string>(Default::EnableSysProcVMStat, "false"));
#endif
#ifdef WITH_JOB_CONTROL
    m_table.insert(std::pair<Default, std::string>(Default::EnableJobControl, "true"));
    m_table.insert(std::pair<Default, std::string>(Default::JCDefaultSuspendTimeout, "1000000"));
    m_table.insert(std::pair<Default, std::string>(Default::JCTriggerGroupOnSignal, "none"));
    m_table.insert(std::pair<Default, std::string>(Default::JCSignalDestination, "none"));
    m_table.insert(std::pair<Default, std::string>(Default::JCSignalPath, "none"));
    m_table.insert(std::pair<Default, std::string>(Default::JCSignalInterface, "none"));
    m_table.insert(std::pair<Default, std::string>(Default::JCSignalEventName, "none"));
#endif
#ifdef WITH_WAKE_LOCK
    m_table.insert(std::pair<Default, std::string>(Default::TCPActiveWakeLock, "false"));
#endif

    m_vals.insert(std::pair<Val, std::string>(Val::True, "true"));
    m_vals.insert(std::pair<Val, std::string>(Val::False, "false"));
    m_vals.insert(std::pair<Val, std::string>(Val::None, "none"));
    m_vals.insert(std::pair<Val, std::string>(Val::ProcAcct, "ProcAcct"));
    m_vals.insert(std::pair<Val, std::string>(Val::ProcInfo, "ProcInfo"));

    m_args.insert(std::pair<Arg, std::string>(Arg::WithEventSource, "WithEventSource"));
  }

  auto getFor(Default key) -> std::string & { return m_table.at(key); }
  auto valFor(Val key) -> std::string & { return m_vals.at(key); }
  auto argFor(Arg key) -> std::string & { return m_args.at(key); }

private:
  std::map<Default, std::string> m_table;
  std::map<Val, std::string> m_vals;
  std::map<Arg, std::string> m_args;
};

static Defaults tkmDefaults{};

} // namespace tkm::monitor
