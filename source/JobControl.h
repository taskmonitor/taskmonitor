/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2024
 * @author    Alin Popa <alin.popa@triboo-tech.ro>
 * @copyright MIT
 * @brief     JobControl Class
 * @details   Manage external processes STOP/CONTINUE signals
 *-
 */

#pragma once

#include <sdbus-c++/IConnection.h>
#include <sdbus-c++/sdbus-c++.h>
#include <string>

#include "Options.h"

#include "../bswinfra/source/AsyncQueue.h"

using namespace bswi::event;

namespace tkm::monitor
{

class JobControl : public std::enable_shared_from_this<JobControl>
{
public:
  enum class Action { Connect, RegisterSignal, Trigger, Cleanup };

  typedef struct Request {
    Action action;
    std::string arg;
  } Request;

public:
  explicit JobControl(const std::shared_ptr<Options> options);
  virtual ~JobControl() = default;

public:
  JobControl(JobControl const &) = delete;
  void operator=(JobControl const &) = delete;

public:
  auto getShared() -> std::shared_ptr<JobControl> { return shared_from_this(); }
  void setEventSource(bool enabled = true);

  bool resetConnection();
  bool triggerCleanup();
  bool registerForGroupSignal();
  bool triggerGroup(const std::string &groupName);

  void setInProgress(const bool state) { m_inProgress = state; }
  bool getInProgress() { return m_inProgress; }

  bool getRegistered() { return m_registered; }
  auto getSDBusObject() -> std::unique_ptr<sdbus::IObject>& { return m_sdbusObject; }

  auto pushRequest(JobControl::Request &request) -> int;

private:
  bool requestHandler(const Request &request);

private:
  std::shared_ptr<AsyncQueue<Request>> m_queue = nullptr;
  std::shared_ptr<Options> m_options = nullptr;
  std::unique_ptr<sdbus::IConnection> m_sdbusConnection = nullptr;
  std::unique_ptr<sdbus::IObject> m_sdbusObject = nullptr;
  std::unique_ptr<sdbus::IProxy> m_sdbusSignalProxy = nullptr;
  std::atomic<bool> m_inProgress = false;
  bool m_registered = false;
};

} // namespace tkm::monitor
