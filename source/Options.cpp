/*-
 * SPDX-License-Identifier: MIT
 *-
 * @date      2021-2022
 * @author    Alin Popa <alin.popa@fxdata.ro>
 * @copyright MIT
 * @brief     Options class
 * @details   Runtime options with defaults
 *-
 */

#include "Options.h"
#include "../bswinfra/source/Logger.h"

using namespace std;

namespace tkm::monitor
{

Options::Options(const string &configFile)
: m_configFile(std::make_shared<bswi::kf::KeyFile>(configFile))
{
  if (m_configFile->parseFile() != 0) {
    logWarn() << "Fail to parse config file: " << configFile;
    m_configFile.reset();
  }
}

auto Options::getStringOrDefault(const std::string &section,
                                 const std::string &prop,
                                 Defaults::Default defval) -> const std::string
{
  if (hasConfigFile()) {
    const optional<string> optProp = m_configFile->getPropertyValue(section, -1, prop);
    return optProp.value_or(tkmDefaults.getFor(defval));
  }
  return tkmDefaults.getFor(defval);
}

auto Options::getStringOrDefaultValidateInterval(const std::string &section,
                                                 const std::string &prop,
                                                 Defaults::Default defval,
                                                 long min,
                                                 long max) -> const std::string
{
  if (hasConfigFile()) {
    const optional<string> optProp = m_configFile->getPropertyValue(section, -1, prop);

    try {
      auto interval = std::stoul(optProp.value_or(tkmDefaults.getFor(defval)));
      if ((min != -1) && (interval < static_cast<unsigned long>(min))) {
        logWarn() << "Option '" << section << "::" << prop << "' not in range. Use default!";
        return tkmDefaults.getFor(defval);
      }
      if ((max != -1) && (interval > static_cast<unsigned long>(max))) {
        logWarn() << "Option '" << section << "::" << prop << "' not in range. Use default!";
        return tkmDefaults.getFor(defval);
      }
    } catch (...) {
      logWarn() << "Option '" << section << "::" << prop << "' not integer. Use default!";
      return tkmDefaults.getFor(defval);
    }

    return optProp.value_or(tkmDefaults.getFor(defval));
  }

  return tkmDefaults.getFor(defval);
}

auto Options::getFor(Key key) -> string const
{
  switch (key) {
  case Key::RuntimeDirectory:
    return getStringOrDefault("monitor", "RuntimeDirectory", Defaults::Default::RuntimeDirectory);
  case Key::LogLevel:
    return getStringOrDefault("monitor", "LogLevel", Defaults::Default::LogLevel);
#ifdef WITH_LXC
  case Key::ContainersPath:
    return getStringOrDefault("monitor", "ContainersPath", Defaults::Default::ContainersPath);
#endif
#ifdef WITH_CGROUPS_CONTEXT
  case Key::CGroupsContextPath:
    return getStringOrDefault(
        "monitor", "CGroupsContextPath", Defaults::Default::CGroupsContextPath);
#endif
  case Key::RxBufferSize:
    return getStringOrDefault("monitor", "RxBufferSize", Defaults::Default::RxBufferSize);
  case Key::TxBufferSize:
    return getStringOrDefault("monitor", "TxBufferSize", Defaults::Default::TxBufferSize);
  case Key::MsgBufferSize:
    return getStringOrDefault("monitor", "MsgBufferSize", Defaults::Default::MsgBufferSize);
  case Key::ProdModeFastLaneInt:
    return getStringOrDefaultValidateInterval(
        "production-mode", "FastLaneInterval", Defaults::Default::ProdModeFastLaneInt, 1000000, -1);
  case Key::ProdModePaceLaneInt:
    return getStringOrDefaultValidateInterval(
        "production-mode", "PaceLaneInterval", Defaults::Default::ProdModePaceLaneInt, 1000000, -1);
  case Key::ProdModeSlowLaneInt:
    return getStringOrDefaultValidateInterval(
        "production-mode", "SlowLaneInterval", Defaults::Default::ProdModeSlowLaneInt, 1000000, -1);
  case Key::ProfModeFastLaneInt:
    return getStringOrDefaultValidateInterval(
        "profiling-mode", "FastLaneInterval", Defaults::Default::ProdModeFastLaneInt, 1000000, -1);
  case Key::ProfModePaceLaneInt:
    return getStringOrDefaultValidateInterval(
        "profiling-mode", "PaceLaneInterval", Defaults::Default::ProdModePaceLaneInt, 1000000, -1);
  case Key::ProfModeSlowLaneInt:
    return getStringOrDefaultValidateInterval(
        "profiling-mode", "SlowLaneInterval", Defaults::Default::ProdModeSlowLaneInt, 1000000, -1);
  case Key::ProfModeIfPath:
    return getStringOrDefault("monitor", "ProfModeIfPath", Defaults::Default::ProfModeIfPath);
  case Key::SelfLowerPriority:
    return getStringOrDefault("monitor", "SelfLowerPriority", Defaults::Default::SelfLowerPriority);
  case Key::ReadProcAtInit:
    return getStringOrDefault("monitor", "ReadProcAtInit", Defaults::Default::ReadProcAtInit);
#ifdef WITH_PROC_EVENT
  case Key::EnableProcEvent:
    return getStringOrDefault("monitor", "EnableProcEvent", Defaults::Default::EnableProcEvent);
  case Key::UpdateOnProcEvent:
    return getStringOrDefault("monitor", "UpdateOnProcEvent", Defaults::Default::UpdateOnProcEvent);
#endif
#ifdef WITH_PROC_ACCT
  case Key::EnableProcAcct:
    return getStringOrDefault("monitor", "EnableProcAcct", Defaults::Default::EnableProcAcct);
#endif
  case Key::EnableTCPServer:
    return getStringOrDefault("monitor", "EnableTCPServer", Defaults::Default::EnableTCPServer);
  case Key::EnableUDSServer:
    return getStringOrDefault("monitor", "EnableUDSServer", Defaults::Default::EnableUDSServer);
#ifdef WITH_STARTUP_DATA
  case Key::EnableStartupData:
    return getStringOrDefault("monitor", "EnableStartupData", Defaults::Default::EnableStartupData);
  case Key::StartupDataCleanupTime:
    return getStringOrDefaultValidateInterval("profiling-mode",
                                              "StartupDataCleanupTime",
                                              Defaults::Default::StartupDataCleanupTime,
                                              3000000,
                                              -1);
#endif
  case Key::EnableProcFDCount:
    return getStringOrDefault("monitor", "EnableProcFDCount", Defaults::Default::EnableProcFDCount);
#ifdef WITH_VM_STAT
  case Key::EnableSysProcVMStat:
    return getStringOrDefault(
        "monitor", "EnableSysProcVMStat", Defaults::Default::EnableSysProcVMStat);
#endif
#ifdef WITH_JOB_CONTROL
  case Key::EnableJobControl:
    return getStringOrDefault("monitor", "EnableJobControl", Defaults::Default::EnableJobControl);
  case Key::JCDefaultSuspendTimeout:
    return getStringOrDefaultValidateInterval("jobcontrol",
                                              "JCDefaultSuspendTimeout",
                                              Defaults::Default::JCDefaultSuspendTimeout,
                                              1000000,
                                              -1);
  case Key::JCTriggerGroupOnSignal:
    return getStringOrDefault(
        "jobcontrol", "JCTriggerGroupOnSignal", Defaults::Default::JCTriggerGroupOnSignal);
  case Key::JCSignalDestination:
    return getStringOrDefault(
        "jobcontrol", "JCSignalDestination", Defaults::Default::JCSignalDestination);
  case Key::JCSignalPath:
    return getStringOrDefault("jobcontrol", "JCSignalPath", Defaults::Default::JCSignalPath);
  case Key::JCSignalInterface:
    return getStringOrDefault(
        "jobcontrol", "JCSignalInterface", Defaults::Default::JCSignalInterface);
  case Key::JCSignalEventName:
    return getStringOrDefault(
        "jobcontrol", "JCSignalEventName", Defaults::Default::JCSignalEventName);
#endif
  case Key::CollectorInactiveTimeout:
    return getStringOrDefaultValidateInterval("monitor",
                                              "CollectorInactivityTimeout",
                                              Defaults::Default::CollectorInactiveTimeout,
                                              3000000,
                                              -1);
  case Key::TCPServerAddress:
    return getStringOrDefault("tcpserver", "ServerAddress", Defaults::Default::TCPServerAddress);
  case Key::TCPServerPort:
    return getStringOrDefault("tcpserver", "ServerPort", Defaults::Default::TCPServerPort);
#ifdef WITH_WAKE_LOCK
  case Key::TCPActiveWakeLock:
    return getStringOrDefault("tcpserver", "ActiveWakeLock", Defaults::Default::TCPActiveWakeLock);
#endif
  case Key::UDSServerSocketPath:
    return getStringOrDefault("udsserver", "SocketPath", Defaults::Default::UDSServerSocketPath);
  case Key::UDSMonitorCollectorInactivity:
    return getStringOrDefault("udsserver",
                              "MonitorCollectorInactivity",
                              Defaults::Default::UDSMonitorCollectorInactivity);
  default:
    logError() << "Unknown option key";
    break;
  }

  throw std::runtime_error("Cannot provide option for key");
}

} // namespace tkm::monitor
